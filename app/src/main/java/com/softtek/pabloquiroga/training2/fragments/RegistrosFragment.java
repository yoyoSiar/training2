package com.softtek.pabloquiroga.training2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.softtek.pabloquiroga.training2.R;
import com.softtek.pabloquiroga.training2.activities.HistoricoDetalleActivity;
import com.softtek.pabloquiroga.training2.entities.Clima;
import com.softtek.pabloquiroga.training2.entities.Registro;
import com.softtek.pabloquiroga.training2.utilities.Adapters.RegistrosAdapter;
import com.softtek.pabloquiroga.training2.utilities.Callback;
import com.softtek.pabloquiroga.training2.utilities.DBAsynck;
import com.softtek.pabloquiroga.training2.utilities.DataHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RegistrosFragment extends Fragment implements AdapterView.OnItemClickListener{

    private static final String ARG_SECTION_NUMBER = "section_number";

    View rootView;
    RegistrosAdapter adapter;
    ListView listView;
    ProgressBar bar;
    TextView textView;

    Registro seleccionado;
    List<Registro>listado;

    public RegistrosFragment() {
        // Required empty public constructor
    }

    public static RegistrosFragment newInstance(int sectionNumber) {
        RegistrosFragment fragment = new RegistrosFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_registros, container, false);

        listado = DataHandler.listado2;
        listView = (ListView)rootView.findViewById(R.id.lv_registros);
        bar = (ProgressBar)rootView.findViewById(R.id.progressbar);
        adapter = new RegistrosAdapter(this.getContext(), new ArrayList<Registro>());
        listView.setAdapter(adapter);
        textView = (TextView)rootView.findViewById(R.id.dbNull);

        getData();

        listView.setOnItemClickListener(this);

        return rootView;
    }

    private void getData(){
        DBAsynck dbAsynck = new DBAsynck(new Callback() {
            @Override
            public void starting() {
            }
            @Override
            public void completed(String res) {
                adapter.clear();

                if(DataHandler.listado != null && !DataHandler.listado.isEmpty()){
                    adapter.addAll(DataHandler.listado);
                }else{
                    textView.setVisibility(View.VISIBLE);
                }

                bar.setVisibility(View.GONE);
            }

            @Override
            public void completedWithErrors(Exception e) {
            }
            @Override
            public void update() {
            }
        });
        dbAsynck.execute();
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        seleccionado = DataHandler.listado2.get(i);
        Intent intent = new Intent(getContext(), HistoricoDetalleActivity.class);
        intent.putExtra("datos",seleccionado);
        startActivity(intent);
    }

}
