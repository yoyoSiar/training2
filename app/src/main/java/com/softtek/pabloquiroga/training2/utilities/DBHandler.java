package com.softtek.pabloquiroga.training2.utilities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Pablo Daniel Quiroga on 17/3/2018.
 */

public class DBHandler extends SQLiteOpenHelper {

    public static final String DB_NAME = "DBregistros";
    public static final int DB_VERSION = 1;
    public static final String DB_TABLA = "registro";
    public static final String COLUMNA_ID = "_id";
    public static final String COLUMNA_FECHA = "fecha";
    public static final String COLUMNA_CLIMA = "datos";

    private String sqlCreate = "create table "
            + DB_TABLA +"("
            + COLUMNA_ID + " integer primary key autoincrement, "
            + COLUMNA_FECHA + " datetime not null, "
            + COLUMNA_CLIMA + " text not null "
            + ");";

    public DBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    public void dropRegistros(SQLiteDatabase db){
        db.execSQL("DROP TABLE IF EXISTS registro"); //elimina la tabla preexistente
        db.execSQL(sqlCreate); //crea la nueva version de la tabla
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS registro"); //elimina la tabla preexistente
        db.execSQL(sqlCreate); //crea la nueva version de la tabla
    }


}
