package com.softtek.pabloquiroga.training2.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.softtek.pabloquiroga.training2.R;
import com.softtek.pabloquiroga.training2.entities.Clima;
import com.softtek.pabloquiroga.training2.entities.Registro;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.List;

public class HistoricoDetalleActivity extends AppCompatActivity {

    TextView registro, localidad, descript, temp, pres, hum;
    Registro reg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico_detalle);

        registro = (TextView)findViewById(R.id.lbl_registro);
        localidad = (TextView)findViewById(R.id.lbl_localidad);
        descript = (TextView)findViewById(R.id.lbl_description);
        temp = (TextView)findViewById(R.id.lbl_temperatura);
        pres = (TextView)findViewById(R.id.lbl_pressure);
        hum = (TextView)findViewById(R.id.lbl_humidity);

        reg = (Registro) getIntent().getExtras().getSerializable("datos");

        setData(reg);
    }

    private void setData(Registro r){

        registro.setText(r.getFecha());

        try {
            JSONObject jsonObject = new JSONObject(r.getClima());
            Clima clima = new Clima(jsonObject);

            String ubi = clima.getName();
            localidad.setText(ubi);
            descript.setText(clima.getWeather().get(0).getDescription());
            temp.setText(String.valueOf((int)clima.getMain().getTemp())+ "°C");
            pres.setText(String.valueOf(clima.getMain().getPressure()));
            hum.setText(String.valueOf(clima.getMain().getHumidity()) + "%");
        }catch (JSONException ex){
            Log.e("JSONException", ex.getMessage()+"");
        }
    }

}
