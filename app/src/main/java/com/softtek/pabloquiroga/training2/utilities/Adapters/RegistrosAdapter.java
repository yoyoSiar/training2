package com.softtek.pabloquiroga.training2.utilities.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.softtek.pabloquiroga.training2.R;
import com.softtek.pabloquiroga.training2.entities.Registro;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pablo Daniel Quiroga on 17/3/2018.
 */

public class RegistrosAdapter extends ArrayAdapter<Registro> {

    List<Registro> list;
    TextView textF;
    TextView textU;

    public RegistrosAdapter(Context context, List<Registro> regs) {
        super(context, 0, regs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(
                    R.layout.registro_item,parent,false);
        }

        textF = (TextView) view.findViewById(R.id.txtfecha);
        textU = (TextView) view.findViewById(R.id.txtubi);

        Registro reg = getItem(position);
        if(reg != null) {
            agregarItem(reg);
        }
        notifyDataSetChanged();
        return view;
    }

    private void agregarItem(Registro registro){
        list = new ArrayList<>();
        list.add(registro);
        for(Registro r: list){
            String fecha = r.getFecha();
            String ubicacion = r.getClima();

            textF.setText(fecha);
            textU.setText(ubicacion);
        }
    }
}
