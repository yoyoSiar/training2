package com.softtek.pabloquiroga.training2.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.softtek.pabloquiroga.training2.R;

public class PronosticoActivity extends AppCompatActivity {

    private final String webUrl = "https://weather.com/es-US/tiempo/hoy/l/ARBA1849:1:AR";
    WebView myWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pronostico);

        myWebView = (WebView) findViewById(R.id.webView);

        onLoadUrl();
    }

    private void onLoadUrl(){
        /*Settings para el webView
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);*/
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl(webUrl);
    }
}
