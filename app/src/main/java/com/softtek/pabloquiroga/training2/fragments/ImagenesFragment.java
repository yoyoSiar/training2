package com.softtek.pabloquiroga.training2.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.softtek.pabloquiroga.training2.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Pablo Daniel Quiroga on 13/3/2018.
 */

public class ImagenesFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    Uri imageUri;
    private static final int PICK_IMAGE = 100;

    //Button button;
    ImageView imageView;
    View rootView;

    public ImagenesFragment() {
    }

    public static ImagenesFragment newInstance(int sectionNumber) {
        ImagenesFragment fragment = new ImagenesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_imagenes, container, false);

        //button = (Button)rootView.findViewById(R.id.btn_change);
        imageView = (ImageView)rootView.findViewById(R.id.imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        return rootView;
    }

    private void openGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            imageView.setImageURI(imageUri);
        }
    }
}
