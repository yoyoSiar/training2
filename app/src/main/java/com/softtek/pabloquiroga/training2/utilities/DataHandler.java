package com.softtek.pabloquiroga.training2.utilities;

import android.database.sqlite.SQLiteDatabase;

import com.softtek.pabloquiroga.training2.entities.Registro;

import java.util.List;

/**
 * Created by pablo.quiroga on 3/16/2018.
 * Demasiadas variables, seria bueno acotarlas
 */

public class DataHandler {
    public static String otros;
    public static double latitud = 0;
    public static double longitud = 0;
    public static String ubicacion = "";
    public static int temperatura = 0;
    public static String fecha = "";
    public static SQLiteDatabase db = null;
    public static List<Registro> listado = null; //para ListView
    public static List<Registro> listado2 = null; //para ver detalles

}
