package com.softtek.pabloquiroga.training2.utilities;

import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.softtek.pabloquiroga.training2.entities.Clima;
import com.softtek.pabloquiroga.training2.entities.Registro;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pablo Daniel Quiroga on 18/3/2018.
 */

public class DBAsynck extends AsyncTask<Void, String, String>{

    private Callback callback;
    Registro registro;

    public DBAsynck(Callback c) {
        this.callback = c;
    }

    @Override
    protected void onPreExecute() {
        callback.starting();
    }
    @Override
    protected String doInBackground(Void... voids) {
        String retorno;
        DataHandler.listado2 = new ArrayList<>();

        DataHandler.listado = obtenerRegistros();
        if (DataHandler.listado != null){
            retorno = String.valueOf(DataHandler.listado.size());
        }else{
            retorno = "fue nulo el retorno de DataHandler";
        }
        return retorno;

    }
    @Override
    protected void onProgressUpdate(String... values) {
        callback.update();
    }
    @Override
    protected void onPostExecute(String s) {
        callback.completed(s);
    }

    private List<Registro> obtenerRegistros(){
        List<Registro> lista = null;
        Cursor cursor = DataHandler.db.rawQuery("SELECT fecha,datos FROM registro ORDER BY fecha DESC", null);
        if(cursor.moveToFirst()){
            lista = new ArrayList<>();

            do {
                String f = cursor.getString(0);
                String d = cursor.getString(1);
                registro = new Registro(f,d); // funciona pero no me agrada
                DataHandler.listado2.add(registro);
                registro = parseResponse(f,d);

                lista.add(registro);
            } while(cursor.moveToNext());
        }
        return lista;
    }



    private Registro parseResponse(String date, String data){
        //No es lindo pero funciona
        //SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        DataHandler.fecha = registro.getFecha();
        String dato = "";
        try{
            JSONObject object = new JSONObject(data);
            Clima clima = new Clima(object);
            String ubicacion = clima.getName();
            int t = (int)clima.getMain().getTemp();
            String temp = String.valueOf(t);
            dato = ubicacion + ", " + temp +"°C";
        }catch (JSONException e){
            Log.e("JSONException", "objeto no puede ser leido como JSON");
        }
        DataHandler.otros = dato;
        registro = new Registro(DataHandler.fecha, DataHandler.otros);

        return registro;
    }
}
