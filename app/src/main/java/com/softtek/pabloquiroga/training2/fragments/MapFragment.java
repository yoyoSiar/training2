package com.softtek.pabloquiroga.training2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.softtek.pabloquiroga.training2.R;
import com.softtek.pabloquiroga.training2.utilities.DataHandler;

public class MapFragment extends Fragment implements OnMapReadyCallback{

    private static final String ARG_SECTION_NUMBER = "section_number";

    View rootView;
    TextView textView;

    double lat, lon;
    String ubi;
    int temp;

    LatLng actual;
    GoogleMap map;
    MapView mapView;

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment newInstance(int sectionNumber) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actual = new LatLng(0,0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_map, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) rootView.findViewById(R.id.mapView);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) { // fragment es visible y fue creado
            cargaDatos();
            mapView.getMapAsync(this);
        }
    }

    private void cargaDatos(){
        this.actual = new LatLng(DataHandler.latitud, DataHandler.longitud);
        this.ubi = DataHandler.ubicacion;
        this.temp = DataHandler.temperatura;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());

        this.map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        Marker marcador = map.addMarker(new MarkerOptions()
                .position(actual)
                .title(ubi)
                .snippet("Temperatura: " + temp + "°C")
        );
        marcador.showInfoWindow();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(actual, 15));

        //preferencias de mapa
        map.getUiSettings().setCompassEnabled(false);
        map.getUiSettings().setAllGesturesEnabled(false);
    }
}
