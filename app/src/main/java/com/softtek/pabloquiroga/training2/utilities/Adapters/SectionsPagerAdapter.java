package com.softtek.pabloquiroga.training2.utilities.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.MenuItem;

import com.softtek.pabloquiroga.training2.R;
import com.softtek.pabloquiroga.training2.fragments.ImagenesFragment;
import com.softtek.pabloquiroga.training2.fragments.MapFragment;
import com.softtek.pabloquiroga.training2.fragments.RegistrosFragment;
import com.softtek.pabloquiroga.training2.fragments.WeatherFragment;

/**
 * Created by Pablo Daniel Quiroga on 13/3/2018.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {


    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f;
        switch (position){
            case (0):
                f = WeatherFragment.newInstance(position);
                break;
            case (1):
                f = MapFragment.newInstance(position);
                break;
            case (2):
                f = RegistrosFragment.newInstance(position);
                break;
            default:
                f = ImagenesFragment.newInstance(position);
                break;
        }
        return f;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
