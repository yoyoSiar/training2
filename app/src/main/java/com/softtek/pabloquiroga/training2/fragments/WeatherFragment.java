package com.softtek.pabloquiroga.training2.fragments;

import android.Manifest;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GnssStatus;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.softtek.pabloquiroga.training2.R;
import com.softtek.pabloquiroga.training2.activities.PronosticoActivity;
import com.softtek.pabloquiroga.training2.entities.Clima;
import com.softtek.pabloquiroga.training2.entities.Coord;
import com.softtek.pabloquiroga.training2.utilities.Callback;
import com.softtek.pabloquiroga.training2.utilities.DBHandler;
import com.softtek.pabloquiroga.training2.utilities.DataHandler;
import com.softtek.pabloquiroga.training2.utilities.GpsAsynk;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class WeatherFragment extends Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    TextView lblDesc;
    TextView lblUbic;
    TextView lblTemp;
    TextView lblPres;
    TextView lblHumedad;
    TextView lblWind;
    Button btnGPS;
    Button btnURL;
    ProgressBar bar;

    View rootView;

    Location location;
    private GoogleApiClient apiClient;
    Clima clima;
    String ubi,des,pres,hum,w,t;
    Coord coord;

    DBHandler dbHandler;
    //SQLiteDatabase db;

    public WeatherFragment() {
        // Required empty public constructor
    }

    public static WeatherFragment newInstance(int sectionNumber) {
        WeatherFragment fragment = new WeatherFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_weather, container, false);

        lblDesc = (TextView)rootView.findViewById(R.id.lbl_desc);
        lblUbic = (TextView)rootView.findViewById(R.id.lbl_ubicacion);
        lblTemp = (TextView)rootView.findViewById(R.id.lbl_temp);
        lblPres = (TextView)rootView.findViewById(R.id.lbl_pres);
        lblHumedad = (TextView)rootView.findViewById(R.id.lbl_humidity);
        lblWind = (TextView)rootView.findViewById(R.id.lbl_wind);
        btnGPS = (Button)rootView.findViewById(R.id.btn_gps);
        btnURL = (Button)rootView.findViewById(R.id.btn_url);
        bar = (ProgressBar)rootView.findViewById(R.id.progressbar);

        btnGPS.setOnClickListener(this);
        btnURL.setOnClickListener(this);

        apiClient = new GoogleApiClient.Builder(this.getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        dbHandler = new DBHandler(this.getContext());
        DataHandler.db = dbHandler.getWritableDatabase();

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_gps:
                doWithGps();
                break;
            case R.id.btn_url:
                doWithUrl();
                return;
        }
    }

    private void doWithGps(){
        getData();
        bar.setVisibility(View.VISIBLE);
    }
    private void doWithUrl(){
        Intent dataIntent = new Intent(getActivity(), PronosticoActivity.class);
        startActivity(dataIntent);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) { // fragment es visible y fue creado
            if(clima != null) {
                setDatos(clima);
            }
        }else{
            coord = null;
        }
    }

    private void getData(){
        updateCoords();

        GpsAsynk gps = new GpsAsynk(new Callback() {
            @Override
            public void starting() {
            }
            @Override
            public void completed(String res) {
                parseResponse(res);
                dbRegister(res);
                bar.setVisibility(View.GONE);
            }
            @Override
            public void completedWithErrors(Exception e) {
            }
            @Override
            public void update() {
            }
        });
        gps.execute(DataHandler.latitud,DataHandler.longitud);
    }

    //estos metodos vienen de Fragment
    @Override
    public void onStart() {
        super.onStart();
        apiClient.connect();
    }
    @Override
    public void onPause() {
        super.onPause();
        //TODO aqui manejar cuando entra en onPause()
    }
    @Override
    public void onResume() {
        super.onResume();
        if (apiClient.isConnected()) {
            //TODO aqui manejar cuando vuelve del onPause()
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        apiClient.disconnect();
    }
    /********/

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        location = LocationServices.FusedLocationApi.getLastLocation(apiClient);


    }

    private void updateCoords(){
        try {
            if (location.getProvider() != null) {
                //if(location != null){
                DataHandler.latitud = location.getLatitude();
                DataHandler.longitud = location.getLongitude();
                //}
            } else {
                Toast.makeText(this.getContext(), "Ubicacion no activa", Toast.LENGTH_LONG);
                return;
            }
        }catch (NullPointerException e){
            showAlert("El movil posee desactivada la ubicacion. Desea cargar ubicacion por defecto o salir?");

        }
    }

    private void parseResponse(String s){
        try {
            JSONObject jsonObject = new JSONObject(s);
            clima = new Clima(jsonObject);

            coord = clima.getCoord();
            setDatos(clima);

            if(coord != null){
                btnGPS.setVisibility(View.GONE);
            }
        }catch (JSONException e){
            Log.e("parseResponse", e.toString()+"");
        }
    }

    //Alta de registro en SQLite
    private void dbRegister(String s){
        Date fecha = Calendar.getInstance().getTime();
        DateFormat fechaHora = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String f = fechaHora.format(fecha);

        String reg = s;

        if(DataHandler.db != null){
            ContentValues elemento = new ContentValues();
            elemento.put("fecha", f);
            elemento.put("datos", reg);
            DataHandler.db.insert("registro", null, elemento);
        }
    }

    private void setDatos(Clima c){
        this.ubi = clima.getName();
        this.des = clima.getWeather().get(0).getDescription();
        int x = (int)clima.getMain().getTemp();
        this.t = String.valueOf(x) + "°C";
        this.pres = String.valueOf(clima.getMain().getPressure());
        this.hum = String.valueOf(String.valueOf(clima.getMain().getHumidity()))+"%";
        this.w = String.valueOf((int)clima.getWind().getSpeed()) + " Km/h";

        lblDesc.setText(des);
        lblUbic.setText(ubi);
        lblTemp.setText(t);
        lblPres.setText(pres);
        lblHumedad.setText(hum);
        lblWind.setText(w);

        DataHandler.ubicacion = ubi;
        DataHandler.temperatura = x;
    }

    @Override
    public void onConnectionSuspended(int i) {
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void showAlert(String text){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this.getContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this.getContext());
        }
        builder.setTitle("Ubicacion Inactiva")
                .setMessage(text)
                .setPositiveButton("Defecto", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DataHandler.latitud = 0;
                        DataHandler.longitud = 0;
                    }
                })
                .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }
}
